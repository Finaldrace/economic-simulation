# README zum Prototypen#

### Wofür ist das Repository? ###

Das Repository enthält den Quellcode für den Prototypen einer Master Thesis.
Eine Buildversion steht zum Download bereit.
Es ist jedoch empfehlenswerter das Projekt direkt in Unity zu öffnen, da man dort mehr Variablen beobachten und GameObjects manipulieren kann.


### Was wird benötigt um den Quellcode zu verwenden? ###

Die Software wurde mit der Unity Engine geschrieben, daher muss Unity installiert sein.
Ebenfalls ist es empfehlenswert eine IDE zu installieren. z.B. Visual Studio. (Kann direkt bei der Unity installation angewählt werden)
Der Prototyp ist auf der Unityversion 2020.3.0f1 entwickelt worden. Eine ältere Version kann nicht verwendet werden.
Eine neuere Version kann zu einem Updateprozess führen, der ggf. die Funktion der Software beeinträchtigt.

In dem Unity Hub kann man anwählen, welche Version man installieren möchte.
Der Link zur Unity Hub Installation: https://unity3d.com/de/get-unity/download 


