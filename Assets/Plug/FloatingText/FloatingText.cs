using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour
{

    public float DestroyTime = 3f;

    private void Start()
    {
        Destroy(this.gameObject, DestroyTime);
    }


    public void SetText(string Text)
    {
        this.GetComponent<TextMesh>().text = Text;
    }
}
