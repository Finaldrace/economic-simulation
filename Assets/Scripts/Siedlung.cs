using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Siedlung : MonoBehaviour
{
    public GameObject prefabZuhause;

    public List<GameObject> Hauser;

    public int reihe = 10;
    public int anzahl = 10;
    public int proReihe = 20;

    void Awake()
    {
        Hauser = new List<GameObject>();
    }

    [ExposeMethodInEditorAttribute]
    public void bauenH�user(int anzahlBurger)
    {
        anzahl = anzahlBurger;
        
        int reihe = 0;
        int z�hler = 0;
        for (int i = 0; i < anzahl; i++)
        {
            if (z�hler == proReihe)
            {
                reihe = reihe + 1;
                z�hler = 0;
            }

            Vector3 position = new Vector3(z�hler * 1.3f, reihe * 1.3f);
            position = position + transform.position;
            GameObject haus = Instantiate(prefabZuhause, position,transform.rotation);
            haus.transform.SetParent(this.transform);
            Hauser.Add(haus);
            z�hler = z�hler + 1;
        }
    }
    public GameObject freiesHaus()
    {
        foreach (GameObject item in Hauser)
        {
            if (item.GetComponent<Zuhause>().bewohnt == false)
            {
                return item;
            }
        }
        Debug.LogWarning("Kein freies Haus verf�gbar");
        return null;
    }

}
