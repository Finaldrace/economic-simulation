﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

namespace EconomicSimulation
{

    public class Burger : MonoBehaviour
    {
        #region Eigenschaften des Bürgers

        [Header("Eigenschaften")]

        [SerializeField]
        public float zufriedenheit;

        [SerializeField]
        List<Bedurfnis> beduerfnisse;

        [SerializeField]
        public float speed = 10.0f;

        [Header("Besitz")]
        [SerializeField]
        List<Gut> guterEigentum;

        [SerializeField]
        public double geldBesitz;
        double arbeitskraftMaximal;
        public double arbeitskraft;

        public bool grundPräferenz_Berücksichtigen = true;

        [Header("Erwartungswerte")]
        [SerializeField]
        public int kreis_Wert;
        [SerializeField]
        public int dreieck_Wert;
        [SerializeField]
        public int quadrat_Wert;
        [SerializeField]
        public int raute_Wert;
        [SerializeField]
        public int funfeck_Wert;

        [SerializeField]
        public int Erwartungswertanpassung;

        [Header("Gesetz")]
        [SerializeField]
        public bool berufswechsel;



        public GameObject target;
        public Gut.Güterarten grundPräferenz = Gut.Güterarten.verbraucht;
        public List<Gut.Güterarten> nachgefragteGüter_Heute;
        public List<Gut.Güterarten> erworbeneGüter_Heute;

        public List<Gut.Güterarten> AngeboteneGüter_Heute;
        public List<Gut.Güterarten> VerkaufteGüter_Heute;

        #endregion


        #region Verlinkungen
        [Header("Verlinkungen")]
        public OutputLog logger;

        public Siedlung siedlung;

        [SerializeField]
        Produktion arbeitgeber;
        [SerializeField]
        GameObject arbeitsplatz;

        [SerializeField]
        Produktion letzterArbeitgeber;

        [SerializeField]
        Marktplatz marktplatz;
        [SerializeField]
        GameObject zuhause;

        [SerializeField]
        List<Gut> verbrauchsliste;
        #endregion
        [SerializeField]
        List<Gut.Güterarten> benoetigteGuter;

        public TextMeshPro Geldbesitz_Text;
        public bool GeldEinblenden = false;

        [SerializeField]
        public Gut.Güterarten unzufrieden_Wechselbereit_zu = Gut.Güterarten.verbraucht;


        public void DatenEingeben(int Geldbesitz,int Kreis_Wert,int Dreieck_Wert,int Quadrat_Wert,int Raute_Wert,int Funfeck_Wert, bool Berufswechsel)
        {
            this.geldBesitz = Geldbesitz;
            this.kreis_Wert = Kreis_Wert;
            this.dreieck_Wert = Dreieck_Wert;
            this.quadrat_Wert = Quadrat_Wert;
            this.raute_Wert = Raute_Wert;
            this.funfeck_Wert = Funfeck_Wert;
            this.berufswechsel = Berufswechsel;
            
        }

        void Start()
        {
            nachgefragteGüter_Heute = new List<Gut.Güterarten>();
            benoetigteGuter = new List<Gut.Güterarten>();
            verbrauchsliste = new List<Gut>();

            marktplatz = GameObject.FindGameObjectWithTag("Marktplatz").GetComponent<Marktplatz>();
            logger = GameObject.FindGameObjectWithTag("Logger").GetComponent<OutputLog>();
            siedlung = GameObject.FindGameObjectWithTag("Siedlung").GetComponent<Siedlung>();
            SuchenHaus(); 
        }

        void Update()
        {
            float step = speed * Time.deltaTime;
            if (target != null)
            {
                transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
            }
            if (GeldEinblenden == true)
            {
                Geldbesitz_Text.text = geldBesitz.ToString();
            }
            else
            {
                Geldbesitz_Text.text = "";
            }
        }

        [ExposeMethodInEditorAttribute]
        public void SuchenHaus()
        {
            zuhause = siedlung.freiesHaus();
            if (zuhause != null)
            {
                zuhause.GetComponent<Zuhause>().bewohnt = true;
                transform.position = zuhause.transform.position;
            }
            else
            {
                Debug.LogWarning("Kein Freies Haus verfügbar");
            }
        }

        #region Produktion

        public void Beruf_Kündigen()
        {
            if (arbeitsplatz != null)
            {
                arbeitsplatz.GetComponent<Arbeitsplatz>().besetzt = false;
                arbeitsplatz = null;
                arbeitgeber.entlasseArbeiter(this);
                letzterArbeitgeber = arbeitgeber;
                arbeitgeber = null;
            }
        }

        [ExposeMethodInEditorAttribute]
        bool Beruf_Wählen()
        {
            if (arbeitgeber != null)
            {
               // if(unzufrieden_Wechselbereit_zu == Gut.Güterarten.verbraucht && grundPräferenz == Gut.Güterarten.verbraucht)
                return false;
            }

            double besterLohn = 0;
            Produktion besterArbeitgeber = null;
            GameObject besterArbeitsplatz = null;

            foreach (GameObject ProduktionObjekt in GameObject.FindGameObjectsWithTag("Produktion"))
            {
                Produktion produktion = ProduktionObjekt.GetComponent<Produktion>();

                //Falls der Bürger bereits dort arbeitet, wird dieser Arbeitgeber übersprungen.
                if (letzterArbeitgeber == produktion && grundPräferenz == Gut.Güterarten.verbraucht)
                {
                    Debug.Log("Das war gerade mein letzter Arbeitgeber");
                    continue;
                }

                if (produktion.ArbeitsplätzeVorhanden() == false)
                {
                    Debug.Log("Es gibt keine Jobs in dieser produktion " + produktion.name);
                    continue;
                }

                if (grundPräferenz != produktion.herstellungsGutPrefab.GetComponent<Gut>().gutArt)
                {
                    if (grundPräferenz_Berücksichtigen == true)
                    {
                        continue;
                    }
                }

                GameObject arbeitsplatz = produktion.freierArbeitsplatz();

                if (arbeitsplatz != null)
                {
                    if (produktion.lohnProKraft > besterLohn)
                    {
                        besterLohn = produktion.lohnProKraft;
                        besterArbeitgeber = produktion;
                        besterArbeitsplatz = arbeitsplatz;
                        arbeitsplatz.GetComponent<Arbeitsplatz>().besetzt = true;
                        grundPräferenz = Gut.Güterarten.verbraucht;
                        //Durch den neuen Arbeitsplatz, verliert der Bürger seine aktuelle Wechselbereitschaft
                        unzufrieden_Wechselbereit_zu = Gut.Güterarten.verbraucht;

                        //Nach der ersten Berufswahl wird die Präferenz nicht 
                        if (grundPräferenz_Berücksichtigen == true)
                            grundPräferenz_Berücksichtigen = false;
                    }
                }
            }

            if (besterArbeitgeber != null)
            {
                arbeitgeber = besterArbeitgeber;
                arbeitsplatz = besterArbeitsplatz;
                besterArbeitgeber.einstellenArbeiter(this.GetComponent<Burger>());
                return true;
            }
            else
            {
                Debug.Log("Kein Beruf vorhanden");
                return false;
            }
        }

        public bool PrüfeObWechselBereit()
        {
            if (unzufrieden_Wechselbereit_zu != Gut.Güterarten.verbraucht)
            {
                return true;
            }
            return false;
        }

        [ExposeMethodInEditorAttribute]
        public void Gehen_Arbeitsplatz()
        {
            target = arbeitsplatz;
        }

        [ExposeMethodInEditorAttribute]
        public void Arbeiten()
        {
            if (arbeitgeber == null)
            {
                Beruf_Wählen();
            }

            if (arbeitgeber != null)
            {
                //double lohn = arbeitgeber.Arbeitstag(arbeitskraft);
                //geldBesitz = geldBesitz + lohn;
                int erarbeitet = 0;

                for (int i = 0; i < arbeitgeber.lohnProKraft; i++)
                {
                    GameObject neuesGut = Instantiate(arbeitgeber.herstellungsGutPrefab, transform.position, transform.rotation);
                    neuesGut.transform.SetParent(transform);
                    neuesGut.name = "Gut_" + arbeitgeber.herstellungsGutPrefab.GetComponent<Gut>().gutArt;
                    guterEigentum.Add(neuesGut.GetComponent<Gut>());
                    erarbeitet = erarbeitet + 1;
                }
                
                logger.Log(this.name + " hat in " + arbeitgeber.name + " gearbeitet und " + erarbeitet + " " + arbeitgeber.herstellungsGutPrefab.GetComponent<Gut>().gutArt + " verdient.");
            }
        }
        #endregion

        #region Marktplatz

        [ExposeMethodInEditorAttribute]
        public void Gehen_Marktplatz()
        {
            target = marktplatz.gameObject;
        }

        [ExposeMethodInEditorAttribute]
        public void BedarfPrüfen()
        {
            benoetigteGuter.Clear();
            foreach (Bedurfnis bedurfnis in beduerfnisse)
            {
                int benoetigteMenge = bedurfnis.menge;
                if (guterEigentum.Count > 0)
                {
                    foreach (Gut gut in guterEigentum)
                    {
                        if (gut.gutArt == bedurfnis.gut && benoetigteMenge > 0)
                        {
                            benoetigteMenge = benoetigteMenge - 1;
                            gut.locked = true;
                        }
                    }
                }
                if (benoetigteMenge > 0)
                {
                    for (int i = 0; i < benoetigteMenge; i++)
                    {
                        benoetigteGuter.Add(bedurfnis.gut);
                    }
                }
            }
        }

        public void Entferne_AlteDaten()
        {
            benoetigteGuter.Clear();
        }

        public void VerkaufbarPrüfen()
        {
            foreach (Gut gut in guterEigentum)
            {
                if (gut.locked == false)
                {
                    float Angebotspreis = 1;
                    float Minimalpreis = 1;
                    switch (gut.gutArt)
                    {
                        case Gut.Güterarten.Kreis:
                            Angebotspreis = kreis_Wert;
                            Minimalpreis = kreis_Wert;
                            break;
                        case Gut.Güterarten.Dreieck:
                            Angebotspreis = dreieck_Wert;
                            Minimalpreis = dreieck_Wert;
                            break;
                        case Gut.Güterarten.Quadrat:
                            Angebotspreis = quadrat_Wert;
                            Minimalpreis = quadrat_Wert;
                            break;
                        case Gut.Güterarten.Raute:
                            Angebotspreis = raute_Wert;
                            Minimalpreis = raute_Wert;
                            break;
                        case Gut.Güterarten.Fünfeck:
                            Angebotspreis = funfeck_Wert;
                            Minimalpreis = funfeck_Wert;
                            break;
                    }
                    AngeboteneGüter_Heute.Add(gut.gutArt);
                    Anbieten(this.GetComponent<Burger>(),gut, Angebotspreis, Minimalpreis);
                }
            }
        }

        public void Kaufversuch()
        {
            foreach (Gut.Güterarten gutart in benoetigteGuter)
            {
                float Nachfragepreis = 1;
                float Minimalpreis = 1;
                switch (gutart)
                {
                    case Gut.Güterarten.Kreis:
                        Nachfragepreis = kreis_Wert;
                        Minimalpreis = kreis_Wert;
                        break;
                    case Gut.Güterarten.Dreieck:
                        Nachfragepreis = dreieck_Wert;
                        Minimalpreis = dreieck_Wert;
                        break;
                    case Gut.Güterarten.Quadrat:
                        Nachfragepreis = quadrat_Wert;
                        Minimalpreis = quadrat_Wert;
                        break;
                    case Gut.Güterarten.Raute:
                        Nachfragepreis = raute_Wert;
                        Minimalpreis = raute_Wert;
                        break;
                    case Gut.Güterarten.Fünfeck:
                        Nachfragepreis = funfeck_Wert;
                        Minimalpreis = funfeck_Wert;
                        break;
                }
                nachgefragteGüter_Heute.Add(gutart);
                Nachfragen(this.GetComponent<Burger>(), gutart, Nachfragepreis,Minimalpreis);
            }
        }
        void Anbieten(Burger Anbieter,Gut Ware, double Angebotspreis, double Minimalpreis)
        {
            ScriptableObject script_angebot = ScriptableObject.CreateInstance("Angebot");
            Angebot angebot = (Angebot)script_angebot;

            angebot.Anbieter = Anbieter;
            angebot.Ware = Ware;
            angebot.Angebotspreis = Angebotspreis;
            angebot.Minimalpreis = Minimalpreis;

            marktplatz.Empfangen_Angebot(angebot);
        }

        void Nachfragen(Burger Nachfrager, Gut.Güterarten WarenArt, double Nachfragepreis, double Maximalpreis)
        {
            ScriptableObject script_nachfrage = ScriptableObject.CreateInstance("Nachfrage");
            Nachfrage nachfrage = (Nachfrage)script_nachfrage;

            nachfrage.Nachfrager = Nachfrager;
            nachfrage.WarenArt = WarenArt;
            nachfrage.Nachfragepreis = Nachfragepreis;
            nachfrage.Maximalpreis = Maximalpreis;

            marktplatz.Empfangen_Nachfrage(nachfrage);
        }

        public Gut WareGeben(Gut Ware)
        {
            guterEigentum.Remove(Ware);
            VerkaufteGüter_Heute.Add(Ware.gutArt);
            return Ware;
        }
        public double GeldGeben(double Summe)
        {
            geldBesitz = geldBesitz - Summe;
            return Summe;
        }

        public void WareEmpfangen(Gut Ware)
        {
            guterEigentum.Add(Ware);
            Ware.transform.position = transform.position;
            Ware.transform.SetParent(this.transform);
            erworbeneGüter_Heute.Add(Ware.gutArt);
        }
        public void GeldEmpfangen(double Summe)
        {
            geldBesitz = geldBesitz + Summe;
        }
        #endregion

        #region Konsumieren

        [ExposeMethodInEditorAttribute]
        public void Gehen_Nachhause()
        {
            target = zuhause;
        }

        [ExposeMethodInEditorAttribute]
        public void Konsumieren()
        {
            //Bedürfnis erfüllen, gut verbrauchen
            foreach (Bedurfnis bedurfnis in beduerfnisse)
            {
                for (int i = 0; i < bedurfnis.menge; i++)
                {
                    foreach (Gut gut in guterEigentum)
                    {
                        if (bedurfnis.gut == gut.gutArt)
                        {
                            bedurfnis.konsumiert = bedurfnis.konsumiert + 1;
                            logger.Log(this.name + " hat " + gut.gutArt + " konsumiert.");
                            //CreateText("Konsumiere" + gut.gutArt.ToString());
                            gut.gutArt = Gut.Güterarten.verbraucht;

                            verbrauchsliste.Add(gut);
                            break;
                        }
                    }
                }  
            }
        }

        [SerializeField]
        public List<Gut> result;

        public void Hinzufügen_Bedurfnis(Bedurfnis bedurfnis)
        {
            beduerfnisse.Add(bedurfnis);
        }

        [ExposeMethodInEditorAttribute]
        public void Entfernen_Verbrauchte()
        {
            guterEigentum.RemoveAll(i => verbrauchsliste.Contains(i));

            for (int i = verbrauchsliste.Count - 1; i >= 0; i--)
            {
                Destroy(verbrauchsliste[i].gameObject);
            }
            verbrauchsliste.Clear();
        }
        #endregion

        #region Regenerieren
        public void Zufriedenheit_Reflektieren()
        {
            int tagesZufriedenheit = 0;
            foreach (Bedurfnis item in beduerfnisse)
            {
                if (item.konsumiert == item.menge)
                {
                    tagesZufriedenheit = tagesZufriedenheit + 1;
                }
                else
                {
                    unzufrieden_Wechselbereit_zu = item.gut;
                }
            }
            zufriedenheit = tagesZufriedenheit;
            logger.Log(this.name + " hat eine Zufriedenheit von " + zufriedenheit*100 + "." );
        }

        public void Wertvorstellung_Reflektieren()
        {
            foreach (Gut.Güterarten angebotenesGut in AngeboteneGüter_Heute.Distinct())
            {
                bool GutVerkauft = false;
                foreach (Gut.Güterarten verkauftesGut in VerkaufteGüter_Heute.Distinct())
                {
                    if (angebotenesGut == verkauftesGut)
                        GutVerkauft = true;
                }
                if (GutVerkauft == false)
                    Erwartungswert_Anpassen(angebotenesGut, -1);
            }

            foreach (Gut.Güterarten nachgefragtesGut in nachgefragteGüter_Heute.Distinct())
            {
                bool GutErworben = false;
                foreach (Gut.Güterarten erworbenesGut in erworbeneGüter_Heute.Distinct())
                {
                    if (nachgefragtesGut== erworbenesGut)
                        GutErworben = true;
                }
                if (GutErworben == false)
                    Erwartungswert_Anpassen(nachgefragtesGut, +1);
            }

            //Listen für den nächsten Tag leeren
            AngeboteneGüter_Heute.Clear();
            erworbeneGüter_Heute.Clear();
            nachgefragteGüter_Heute.Clear();
            VerkaufteGüter_Heute.Clear();
        }

        public void Erwartungswert_Anpassen(Gut.Güterarten güterArt, int richtung)
        {
            int angepassterWert = 0;
            switch (güterArt)
            {
                case Gut.Güterarten.Kreis:
                    kreis_Wert = kreis_Wert + (Erwartungswertanpassung * richtung);
                    angepassterWert = kreis_Wert;
                    break;
                case Gut.Güterarten.Dreieck:
                    dreieck_Wert = dreieck_Wert + (Erwartungswertanpassung * richtung);
                    angepassterWert = dreieck_Wert;
                    break;
                case Gut.Güterarten.Quadrat:
                    quadrat_Wert = quadrat_Wert + (Erwartungswertanpassung * richtung);
                    angepassterWert = quadrat_Wert;
                    break;
                case Gut.Güterarten.Raute:
                    raute_Wert = raute_Wert + (Erwartungswertanpassung * richtung);
                    angepassterWert = raute_Wert;
                    break;
                case Gut.Güterarten.Fünfeck:
                    funfeck_Wert = funfeck_Wert + (Erwartungswertanpassung * richtung);
                    angepassterWert = funfeck_Wert;
                    break;
            }
            logger.Log(gameObject.name + " hat seinen Erwartungswert für die Güterart " + güterArt + "auf " + angepassterWert + " angepasst.");
        }

        public void Zufriedenheit_Darstellen()
        {
            if (zufriedenheit == beduerfnisse.Count)
            {
                GetComponent<SpriteRenderer>().color = Color.green;
            }
            else if (zufriedenheit <= beduerfnisse.Count - beduerfnisse.Count)
            {
                GetComponent<SpriteRenderer>().color = Color.red;
            }
            else if (zufriedenheit <= beduerfnisse.Count - 1 )
            {
                GetComponent<SpriteRenderer>().color = Color.yellow;
            }
        }

        public float Get_Guterbesitz()
        {
            return guterEigentum.Count;
        }

        public void Regenerieren()
        {
            arbeitskraft = arbeitskraftMaximal;

            foreach (Bedurfnis item in beduerfnisse)
            {
                item.konsumiert = 0;
            }
            logger.Log(this.name + " hat seine Arbeitskraft und Bedürfnisse regeneriert");
        }
        #endregion
    }
}
