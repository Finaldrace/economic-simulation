﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


namespace EconomicSimulation
{
    public class Produktion : MonoBehaviour
    {
        [SerializeField]
        List<Burger> Arbeiter;

        [SerializeField]
        float Arbeitsbelastung_Maximal;

        [SerializeField]
        float Arbeitsbelastung_Minimal;

        [SerializeField]
        public int anzahlArbeitsplätze;

        [SerializeField]
        public GameObject herstellungsGutPrefab;

        [SerializeField]
        double herstellungsAufwand;

        [SerializeField]
        public double lohnProKraft;

       // [SerializeField]
        //double Gut;

        [SerializeField]
        double tagesArbeitskraft;

        [SerializeField]
        double tagesanzahlProdukte;

        [SerializeField]
        List<Gut> güterEigentum;

        public GameObject prefabArbeitsplatz;

        public List<GameObject> Arbeitsplätze;
        void Start()
        {
            güterEigentum = new List<Gut>();
            schaffeArbeitsplätze();
            GetComponent<TextMeshPro>().text = gameObject.name;
        }
        void Update()
        {

        }
        public GameObject freierArbeitsplatz()
        {
            foreach (GameObject item in Arbeitsplätze)
            {
                if (item.GetComponent<Arbeitsplatz>().besetzt == false)
                {
                    return item;
                }
            }
            Debug.LogWarning("Kein freier Arbeitsplatz bei "+ gameObject.name + " verfügbar");
            return null;
        }

        public bool ArbeitsplätzeVorhanden()
        {
            if(Arbeiter.Count < anzahlArbeitsplätze)
                return true;
            else
                return false;
        }

        [ExposeMethodInEditorAttribute]
        public void schaffeArbeitsplätze()
        {
            int reihe = 0;
            int zähler = 0;
            for (int i = 0; i < anzahlArbeitsplätze; i++)
            {
                if (zähler == 25)
                {
                    reihe = reihe + 1;
                    zähler = 0;
                }

                Vector3 position = new Vector3(zähler * 1.3f, reihe * 1.3f);
                position = position + transform.position;
                GameObject haus = Instantiate(prefabArbeitsplatz, position, transform.rotation);
                haus.transform.SetParent(this.transform);
                Arbeitsplätze.Add(haus);
                zähler = zähler + 1;
            }
        }
        public void einstellenArbeiter(Burger arbeiter)
        {
            Arbeiter.Add(arbeiter);
        }

        public void entlasseArbeiter(Burger arbeiter)
        {
            Arbeiter.Remove(arbeiter);
        }

        public double Arbeitstag(double Arbeitskraft)
        {
            tagesArbeitskraft = tagesArbeitskraft + Arbeitskraft;

            return Arbeitskraft * lohnProKraft;
        }

        [ExposeMethodInEditorAttribute]
        public void Produzieren()
        {
            if (tagesArbeitskraft > herstellungsAufwand)
            {
                tagesanzahlProdukte = tagesArbeitskraft / herstellungsAufwand;
                tagesanzahlProdukte = Math.Floor(tagesanzahlProdukte);
                double restArbeitskraft = tagesArbeitskraft - herstellungsAufwand * tagesanzahlProdukte;

                tagesArbeitskraft = restArbeitskraft;

                for (int i = 0; i < tagesanzahlProdukte; i++)
                {
                    GameObject gut = Instantiate(herstellungsGutPrefab);
                    güterEigentum.Add(gut.GetComponent<Gut>());

                    gut.transform.position = transform.position;
                    gut.transform.SetParent(this.transform);
                }
            }
        }
    }
}
