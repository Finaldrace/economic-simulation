using System.Collections;
using System.Collections.Generic;
using Com.LuisPedroFonseca.ProCamera2D;
using TMPro;
using UnityEngine;

public class OutputLog : MonoBehaviour
{

    public TextMeshProUGUI mesh ;
    public CanvasGroup canvasGroup;
    public List<string> Messages;
    public GameObject Logg;
    public GameObject LogEntryPrefab;
    public GameObject ListObject;

    public Camera cam;
    public int maxPufferMessages = 500;

    void Start()
    {
        canvasGroup.GetComponent<CanvasGroup>();
    }

    public void ToggleVisibility()
    {
        if (canvasGroup.alpha == 1)
        {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
            cam.GetComponent<ProCamera2DPanAndZoom>().DisableOverUGUI = false;

        }
        else
        {
            canvasGroup.alpha = 1;
            canvasGroup.blocksRaycasts = true;
            cam.GetComponent<ProCamera2DPanAndZoom>().DisableOverUGUI = true;
            GenerateEntrys();
        }
    }

    public void AddEntry(string Text)
    {
        GameObject entry = Instantiate(LogEntryPrefab, ListObject.transform);
        entry.transform.localScale = new Vector3(1, 1, 1);
        entry.GetComponent<TextMeshProUGUI>().text = Text;
        entry.transform.SetParent(ListObject.transform);
    }

    void Update()
    {
        if (canvasGroup.alpha == 1 )
            GenerateEntrys();
    }

    [ExposeMethodInEditor]
    public void ClearMessages()
    {
        Messages.Clear();

        foreach (Transform child in ListObject.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void GenerateEntrys()
    {
        foreach (string item in Messages)
        {
            AddEntry(item);
        }
        Messages.Clear();
    }

    public void ToggleView()
    {
        mesh.enabled = !mesh.enabled;
    }

    public void UpdateView()
    {
        mesh.text = "";
        if (mesh.enabled == true)
        {
            foreach (string message in Messages)
            {
                mesh.text = mesh.text + message + "\n";
            }
        }
    }

    public void Log(string Text)
    {
        Messages.Add(Text);

        if (Messages.Count > maxPufferMessages)
            Messages.RemoveAt(0);

    }
    public void clear()
    {
        Messages.Clear();
        UpdateView();
    }
}
