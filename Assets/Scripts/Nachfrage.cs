using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EconomicSimulation
{
    public class Nachfrage : ScriptableObject
    {
        public enum Nachfragestatus
        {
            Offen,
            Abgelehnt,
            Akzeptiert,
            Abgeschlossen
        }

        public Nachfragestatus Status;
        public Burger Nachfrager;
        public Gut.GŁterarten WarenArt;
        public double Nachfragepreis;
        public double Maximalpreis;
        void Start()
        {
            Status = Nachfragestatus.Offen;
        }
    }
}