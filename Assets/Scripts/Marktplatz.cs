﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EconomicSimulation
{
    public class Marktplatz : MonoBehaviour
    {
        [SerializeField]
        List<Angebot> angebote;
        [SerializeField]
        List<Nachfrage> nachfragen;

        [SerializeField]
        List<Transaktion> transaktionen;

        public GameObject gruppeNachfragen;

        public GameObject gruppeAngebote;

        public GameObject gruppeTransaktionen;


        public GameObject prefabNachfrage;

        public GameObject prefabAngebot;

        public GameObject prefabTransaktion;

        [SerializeField]
        public bool SchuldenVerbieten = false;

        public void Entfernen_AlteDaten()
        {
            nachfragen.Clear();
            angebote.Clear();
            transaktionen.Clear();

            foreach (Transform child in gruppeTransaktionen.transform)
            {
                Destroy(child.gameObject);
            }
        }
        public void Bearbeiten_Anfragen()
        {
            foreach (Nachfrage nachfrage in nachfragen)
            {
                foreach (Angebot angebot in angebote)
                {
                    if (nachfrage.WarenArt == angebot.Ware.gutArt && nachfrage.Status != Nachfrage.Nachfragestatus.Akzeptiert && angebot.Status != Angebot.Angebotsstatus.Akzeptiert)
                    {
                        if (nachfrage.Nachfragepreis == angebot.Angebotspreis)
                        {
                            if (SchuldenVerbieten == true)
                            {
                                if (nachfrage.Nachfrager.geldBesitz < nachfrage.Nachfragepreis)
                                {
                                    Debug.Log("Der Nachfrager besitzt nicht genug Geld. er hat nur " + nachfrage.Nachfrager.geldBesitz);
                                    continue;
                                }
                            }
                            nachfrage.Status = Nachfrage.Nachfragestatus.Akzeptiert;
                            angebot.Status = Angebot.Angebotsstatus.Akzeptiert;


                            GameObject ObjectTransaktion = Instantiate(prefabTransaktion);
                            ObjectTransaktion.transform.SetParent(gruppeTransaktionen.transform);

                            Transaktion transaktion = ObjectTransaktion.GetComponent<Transaktion>();
                            transaktion.nachfrage = nachfrage;
                            transaktion.angebot = angebot;
                            transaktion.summe = angebot.Angebotspreis;
                            transaktion.ware = angebot.Ware;
                            transaktionen.Add(transaktion);
                        }
                        else if(nachfrage.Maximalpreis <= angebot.Minimalpreis)
                        {
                            if (SchuldenVerbieten == true)
                            {
                                if (nachfrage.Nachfrager.geldBesitz < angebot.Minimalpreis)
                                {
                                    Debug.Log("Der Nachfrager besitzt nicht genug Geld. er hat nur " + nachfrage.Nachfrager.geldBesitz);
                                    continue;
                                }
                            }

                            nachfrage.Status = Nachfrage.Nachfragestatus.Akzeptiert;
                            angebot.Status = Angebot.Angebotsstatus.Akzeptiert;

                            GameObject ObjectTransaktion = Instantiate(prefabTransaktion);
                            ObjectTransaktion.transform.SetParent(gruppeTransaktionen.transform);

                            Transaktion transaktion = ObjectTransaktion.GetComponent<Transaktion>();
                            transaktion.nachfrage = nachfrage;
                            transaktion.angebot = angebot;
                            transaktion.summe = angebot.Minimalpreis;
                            transaktion.ware = angebot.Ware;
                            transaktionen.Add(transaktion);
                        }
                    }
                }
            }
            Abschließen_Transaktion();
        }

        public void Empfangen_Angebot(Angebot angebot)
        {
            angebote.Add(angebot);
            logger.Log(angebot.Anbieter.name + " bietet " + angebot.Ware.gutArt + " zu " + angebot.Angebotspreis + " an." );
        }

        public void Empfangen_Nachfrage(Nachfrage nachfrage)
        {
            nachfragen.Add(nachfrage);
            logger.Log(nachfrage.Nachfrager.name + " fragt " + nachfrage.WarenArt + " zu " + nachfrage.Nachfragepreis + " an.");
        }

        public OutputLog logger;
        void Abschließen_Transaktion()
        {
            foreach (Transaktion transaktion in transaktionen)
            {
                Burger anbieter = transaktion.angebot.Anbieter;
                Burger nachfrager = transaktion.nachfrage.Nachfrager;
                Debug.Log("anbieter " + anbieter + " nachfrager " + nachfrager);
                anbieter.GeldEmpfangen(transaktion.summe);
                nachfrager.GeldGeben(transaktion.summe);

                nachfrager.WareEmpfangen(anbieter.WareGeben(transaktion.ware));
                logger.Log(nachfrager.name + " hat " + transaktion.ware.gutArt + " von " + anbieter.name +" für " + transaktion.summe + " gekauft.");
            }
        }


    }
}