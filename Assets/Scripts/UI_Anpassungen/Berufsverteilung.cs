using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Berufsverteilung : MonoBehaviour
{
    public Slider kreisSlider;
    public Slider dreieckSlider;
    public Slider quadratSlider;
    public Slider rauteSlider;
    public Slider fünfeckSlider;

    public float maxBurger;
    public float currentBurger;
    public Slider anzahlBurger;
    public Slider anzahlGuter;

    public float gleichverteilung;
    public bool ChangeBlocked = false;

    void Start()
    {
        changeSlider();
    }

    public void changeSlider()
    {
        if (ChangeBlocked == false)
        {
            maxBurger = anzahlBurger.value;

            currentBurger = kreisSlider.value + dreieckSlider.value + quadratSlider.value + rauteSlider.value + fünfeckSlider.value;

            kreisSlider.maxValue = maxBurger - currentBurger + kreisSlider.value;
            dreieckSlider.maxValue = maxBurger - currentBurger + dreieckSlider.value;
            quadratSlider.maxValue = maxBurger - currentBurger + quadratSlider.value;
            rauteSlider.maxValue = maxBurger - currentBurger + rauteSlider.value;
            fünfeckSlider.maxValue = maxBurger - currentBurger + fünfeckSlider.value;
        }
    }
    public void resetSliderValue()
    {
        kreisSlider.value = 0;
        dreieckSlider.value = 0;
        quadratSlider.value = 0;
        rauteSlider.value = 0;
        fünfeckSlider.value = 0;
    }

    public void ArbeitGleichVerteilen()
    {
        maxBurger = anzahlBurger.value;
        float guter = anzahlGuter.value;

        ChangeBlocked = true;

        gleichverteilung = maxBurger / guter;


        float rest =  gleichverteilung - Mathf.FloorToInt(gleichverteilung);
        rest = rest * guter;

        gleichverteilung = Mathf.FloorToInt(gleichverteilung);

        if (guter > 4)
        {
            fünfeckSlider.value = gleichverteilung;
        }
        if (guter > 3)
        {
            rauteSlider.value = gleichverteilung;
        }
        if (guter > 2)
        {
            quadratSlider.value = gleichverteilung;
        }
        if (guter > 1)
        {
            dreieckSlider.value = gleichverteilung;
        }

        kreisSlider.value = gleichverteilung;
        kreisSlider.value = kreisSlider.value + rest;

        kreisSlider.maxValue = kreisSlider.value;
        dreieckSlider.maxValue = dreieckSlider.value;
        quadratSlider.maxValue = quadratSlider.value;
        rauteSlider.maxValue = rauteSlider.value;
        fünfeckSlider.maxValue = fünfeckSlider.value;

        ChangeBlocked = false;
    }
}
