using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Geldbesitz : MonoBehaviour
{
    public Slider geld;
    public Slider guter;

    public void changeSlider()
    {
        geld.value = guter.value - 1;
    }

}
