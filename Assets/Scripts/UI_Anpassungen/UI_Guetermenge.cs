using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Guetermenge : MonoBehaviour
{
    [Header("Produktion")]
    public GameObject Gut1_Text;
    public GameObject Gut1_Slider;

    public GameObject Gut2_Text;
    public GameObject Gut2_Slider;

    public GameObject Gut3_Text;
    public GameObject Gut3_Slider;

    public GameObject Gut4_Text;
    public GameObject Gut4_Slider;

    public GameObject Gut5_Text;
    public GameObject Gut5_Slider;

    [Header("Berufsverteilung")]
    public GameObject Gut1_Arbeiter_Text;
    public GameObject Gut1_Arbeiter_Slider;

    public GameObject Gut2_Arbeiter_Text;
    public GameObject Gut2_Arbeiter_Slider;

    public GameObject Gut3_Arbeiter_Text;
    public GameObject Gut3_Arbeiter_Slider;

    public GameObject Gut4_Arbeiter_Text;
    public GameObject Gut4_Arbeiter_Slider;

    public GameObject Gut5_Arbeiter_Text;
    public GameObject Gut5_Arbeiter_Slider;

    [Header("Erwartungswerte")]

    public GameObject Gut1_Wertvorstellung_Text;
    public GameObject Gut1_Wertvorstellung_Slider;

    public GameObject Gut2_Wertvorstellung_Text;
    public GameObject Gut2_Wertvorstellung_Slider;

    public GameObject Gut3_Wertvorstellung_Text;
    public GameObject Gut3_Wertvorstellung_Slider;

    public GameObject Gut4_Wertvorstellung_Text;
    public GameObject Gut4_Wertvorstellung_Slider;

    public GameObject Gut5_Wertvorstellung_Text;
    public GameObject Gut5_Wertvorstellung_Slider;


    public VerticalLayoutGroup verticalLayoutGroup;

    IEnumerator UpdateRect()
    {
        if (verticalLayoutGroup != null)
        {
            verticalLayoutGroup.enabled = false;
            yield return new WaitForSeconds(0.1F);
            verticalLayoutGroup.enabled = true;
        }
    }
    public void UpdateGueterUI()
    {
        float anzahl = GetComponent<Slider>().value;

        //Produktion
        Gut5_Text.gameObject.SetActive(true);
        Gut5_Slider.gameObject.SetActive(true);
        Gut5_Slider.GetComponent<Slider>().value = anzahl;
        Gut4_Text.gameObject.SetActive(true);
        Gut4_Slider.gameObject.SetActive(true);
        Gut4_Slider.GetComponent<Slider>().value = anzahl;
        Gut3_Text.gameObject.SetActive(true);
        Gut3_Slider.gameObject.SetActive(true);
        Gut3_Slider.GetComponent<Slider>().value = anzahl;
        Gut2_Text.gameObject.SetActive(true);
        Gut2_Slider.gameObject.SetActive(true);
        Gut2_Slider.GetComponent<Slider>().value = anzahl;
        Gut1_Text.gameObject.SetActive(true);
        Gut1_Slider.gameObject.SetActive(true);
        Gut1_Slider.GetComponent<Slider>().value = anzahl;

        //Berufsverteilung
        Gut1_Arbeiter_Text.SetActive(true);
        Gut1_Arbeiter_Slider.SetActive(true);

        Gut2_Arbeiter_Text.SetActive(true);
        Gut2_Arbeiter_Slider.SetActive(true);

        Gut3_Arbeiter_Text.SetActive(true);
        Gut3_Arbeiter_Slider.SetActive(true);

        Gut4_Arbeiter_Text.SetActive(true);
        Gut4_Arbeiter_Slider.SetActive(true);

        Gut5_Arbeiter_Text.SetActive(true);
        Gut5_Arbeiter_Slider.SetActive(true);

        //Erwartungswerte

        Gut1_Wertvorstellung_Text.SetActive(true);
        Gut1_Wertvorstellung_Slider.SetActive(true);

        Gut2_Wertvorstellung_Text.SetActive(true);
        Gut2_Wertvorstellung_Slider.SetActive(true);

        Gut3_Wertvorstellung_Text.SetActive(true);
        Gut3_Wertvorstellung_Slider.SetActive(true);

        Gut4_Wertvorstellung_Text.SetActive(true);
        Gut4_Wertvorstellung_Slider.SetActive(true);

        Gut5_Wertvorstellung_Text.SetActive(true);
        Gut5_Wertvorstellung_Slider.SetActive(true);

        if (anzahl < 5)
        {
            Gut5_Text.gameObject.SetActive(false);
            Gut5_Slider.gameObject.SetActive(false);

            Gut5_Arbeiter_Text.SetActive(false);
            Gut5_Arbeiter_Slider.SetActive(false);

            Gut5_Wertvorstellung_Text.SetActive(false);
            Gut5_Wertvorstellung_Slider.SetActive(false);
        }
        if (anzahl < 4)
        {
            Gut4_Text.gameObject.SetActive(false);
            Gut4_Slider.gameObject.SetActive(false);

            Gut4_Arbeiter_Text.SetActive(false);
            Gut4_Arbeiter_Slider.SetActive(false);

            Gut4_Wertvorstellung_Text.SetActive(false);
            Gut4_Wertvorstellung_Slider.SetActive(false);

        }
        if (anzahl < 3)
        {
            Gut3_Text.gameObject.SetActive(false);
            Gut3_Slider.gameObject.SetActive(false);

            Gut3_Arbeiter_Text.SetActive(false);
            Gut3_Arbeiter_Slider.SetActive(false);

            Gut3_Wertvorstellung_Text.SetActive(false);
            Gut3_Wertvorstellung_Slider.SetActive(false);

        }
        if (anzahl < 2)
        {
            Gut2_Text.gameObject.SetActive(false);
            Gut2_Slider.gameObject.SetActive(false);

            Gut2_Arbeiter_Text.SetActive(false);
            Gut2_Arbeiter_Slider.SetActive(false);

            Gut2_Wertvorstellung_Text.SetActive(false);
            Gut2_Wertvorstellung_Slider.SetActive(false);

        }

        StartCoroutine(UpdateRect());
    }
}
