﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EconomicSimulation

{
    public class Transaktion : MonoBehaviour
    {
        public Angebot angebot;
        public Nachfrage nachfrage;

        public double summe;
        public Gut ware;
    }
}
