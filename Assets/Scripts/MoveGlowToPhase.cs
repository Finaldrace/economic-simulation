using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace EconomicSimulation
{
    public class MoveGlowToPhase : MonoBehaviour
    {
        public GameObject Produktionsphase;
        public GameObject Marktphase;
        public GameObject Konsumphase;
        public GameObject Regenerationsphase;

        public float speed = 1200;

        public GameObject target;

        void Update()
        {
            // Move our position a step closer to the target.
            float step = speed * Time.deltaTime; // calculate distance to move
            if (target != null)
            {
                transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
            }
        }
        public void MoveToPhase(Ablaufzyklus.Phasen phase)
        {
            switch (phase)
            {
                case Ablaufzyklus.Phasen.Produktionsphase:
                    target = Produktionsphase;

                    break;
                case Ablaufzyklus.Phasen.Marktphase:

                    target = Marktphase;
                    break;
                case Ablaufzyklus.Phasen.Konsumphase:

                    target = Konsumphase;
                    break;
                case Ablaufzyklus.Phasen.Regenerationsphase:
                    target = Regenerationsphase;
                    break;
                default:
                    break;
            }
        }
    }
}