﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace EconomicSimulation
{
public class Ablaufzyklus : MonoBehaviour
{
    public enum Phasen
    {
        Produktionsphase,
        Marktphase,
        Konsumphase,
        Regenerationsphase //Rechtschreibfehler Klassendiagram
    }
    [SerializeField] 
    Phasen tagesphase;

    [SerializeField] 
    List<Burger> gesellschaft;

    [SerializeField]
    List<Produktion> industrie;

    [SerializeField]
    public Analysewerkzeug analysewerkzeug;

    [SerializeField]
    int Tag = 0;

    [SerializeField]
    TextMeshProUGUI tagesAnzeige;

    public MoveGlowToPhase moveGlowToPhase;

    [SerializeField]
    TextMeshProUGUI phasenanzeige;

    [SerializeField]
    Marktplatz marktPlatz;

    [SerializeField]
    public bool erwartungswerte;

    void Start()
    {
    tagesphase = Phasen.Produktionsphase;
        
    }

[ExposeMethodInEditorAttribute]
    public void ErfasseObjekte()
    {
        gesellschaft = new List<Burger>();
        industrie = new List<Produktion>();

        foreach(GameObject einwohner in GameObject.FindGameObjectsWithTag("Burger")) {
             gesellschaft.Add(einwohner.GetComponent<Burger>());
         }

        foreach (GameObject produktion in GameObject.FindGameObjectsWithTag("Produktion"))
        {
            industrie.Add(produktion.GetComponent<Produktion>());
        }
    }

    void Update()
    {
        tagesAnzeige.text = Tag.ToString();
        moveGlowToPhase.MoveToPhase(tagesphase);
    }

    public void Schnelldurchlauf_Tag()
    {
        for (int i = 0; i < 4; i++)
        {
            Starten();
            Wechseln();
        }
    }

[ExposeMethodInEditorAttribute]
    public void Wechseln()
    {
        switch (tagesphase)
        {
            case Phasen.Produktionsphase:
                tagesphase = Phasen.Marktphase;
                break;
            case Phasen.Marktphase:
                tagesphase = Phasen.Konsumphase;  
                break;
            case Phasen.Konsumphase:
                tagesphase = Phasen.Regenerationsphase;
                break;
            case Phasen.Regenerationsphase:
                tagesphase = Phasen.Produktionsphase;
                Tag = Tag + 1;
                break;
        }
    }

[ExposeMethodInEditorAttribute]
    public void Starten()
    {
        switch (tagesphase)
        {
            case Phasen.Produktionsphase:

            foreach (Burger burger in gesellschaft)
            {
                if (burger.PrüfeObWechselBereit() == true && burger.berufswechsel == true)
                {
                    burger.Beruf_Kündigen();
                }
                burger.Arbeiten();
                burger.Gehen_Arbeitsplatz();
            }

            foreach (Produktion produktion in industrie)
            {
                produktion.Produzieren();
            }
            break;

            case Phasen.Marktphase:

            marktPlatz.Entfernen_AlteDaten();

            foreach (Burger burger in gesellschaft)
            {
                burger.Gehen_Marktplatz();
                burger.Entferne_AlteDaten();
                burger.BedarfPrüfen();
                burger.VerkaufbarPrüfen();
                burger.Kaufversuch();
            }
            marktPlatz.Bearbeiten_Anfragen();

            break;
            case Phasen.Konsumphase:

            foreach (Burger burger in gesellschaft)
            {
                burger.Gehen_Nachhause();

                burger.Konsumieren();
                burger.Entfernen_Verbrauchte();
            }

            break;
            case Phasen.Regenerationsphase:

            analysewerkzeug.Reset_Tag();

            foreach (Burger burger in gesellschaft)
            {
                burger.Gehen_Nachhause();
                burger.Zufriedenheit_Reflektieren();
                burger.Zufriedenheit_Darstellen();
                if (erwartungswerte == true)
                {
                    burger.Wertvorstellung_Reflektieren();
                }
                
                burger.Regenerieren();
                analysewerkzeug.Erfasse_BurgerBesitz(burger.zufriedenheit, burger.Get_Guterbesitz(), Convert.ToSingle(burger.geldBesitz));
                analysewerkzeug.Erfasse_BurgerVorstellungen(burger.kreis_Wert, burger.dreieck_Wert, burger.quadrat_Wert, burger.raute_Wert, burger.funfeck_Wert);
            }
            analysewerkzeug.BerechneDurchschnitte();
            analysewerkzeug.BefullenStatistik();
            break;
        }
    }

}
}
