﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EconomicSimulation
{
    public class Gut : MonoBehaviour
    {
        public enum Güterarten
        {
            Kreis,
            Dreieck,
            Quadrat,
            Raute,
            Fünfeck,
            verbraucht
        }
        
        [SerializeField]
        public Güterarten gutArt;

        public SpriteRenderer spriteRenderer;
        public bool locked = false;

        void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void SetVisible()
        {
            spriteRenderer.enabled = true;
        }
        public void SetInisible()
        {
            spriteRenderer.enabled = false;
        }
    }
}
