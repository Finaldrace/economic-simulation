﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EconomicSimulation
{
    public class Simulationseinstellung : MonoBehaviour
    {

        #region Parameter

        [Header("Mengen")]
        [SerializeField]
        int anzahlBurger;
        [SerializeField]
        int anzahlGuter;
        [SerializeField]
        float geldbesitz;

        [Header("Produktion")]
        [SerializeField]
        float Lohn_Kreis;
        [SerializeField]
        float Lohn_Dreieck;
        [SerializeField]
        float Lohn_Quadrat;
        [SerializeField]
        float Lohn_Raute;
        [SerializeField]
        float Lohn_Funfeck;

        [Header("Berufsverteilung")]
        [SerializeField]
        float arbeiter_Kreis;
        [SerializeField]
        float arbeiter_Dreieck;
        [SerializeField]
        float arbeiter_Quadrat;
        [SerializeField]
        float arbeiter_Raute;
        [SerializeField]
        float arbeiter_Funfeck;

        [Header("Erwartungswerte")]
        [SerializeField]
        float wertvorstellung_Kreis;
        [SerializeField]
        float wertvorstellung_Dreieck;
        [SerializeField]
        float wertvorstellung_Quadrat;
        [SerializeField]
        float wertvorstellung_Raute;
        [SerializeField]
        float wertvorstellung_Funfeck;

        [SerializeField]
        int erwarungswertanpassung;

        [Header("Gesetze")]
        [SerializeField]
        public bool berufswechsel;
        [SerializeField]
        public bool schulden;
        [SerializeField]
        public bool erwartungswerte;

        [Header("Güter - Prefabs")]
        [SerializeField]
        GameObject herstellungsgut_Prefab_Kreis;
        [SerializeField]
        GameObject herstellungsgut_Prefab_Dreieck;
        [SerializeField]
        GameObject herstellungsgut_Prefab_Quadrat;
        [SerializeField]
        GameObject herstellungsgut_Prefab_Raute;
        [SerializeField]
        GameObject herstellungsgut_Prefab_Funfeck;

        #endregion

        #region Verlinkungen
        [Header("Verlinkungen")]
        [SerializeField]
        GameObject gesellschaft;
        [SerializeField]
        GameObject industrie;
        [SerializeField]
        GameObject marktplatz;

        [SerializeField]
        Siedlung siedlung;

        [SerializeField]
        GameObject burgerPrefab;

        [SerializeField]
        GameObject produktionPrefab;

        [SerializeField]
        GameObject Bedürfnis_Kreis_Prefab;

        [SerializeField]
        GameObject Bedürfnis_Dreieck_Prefab;

        [SerializeField]
        GameObject Bedürfnis_Quadrat_Prefab;

        [SerializeField]
        GameObject Bedürfnis_Raute_Prefab;

        [SerializeField]
        GameObject Bedürfnis_Funfeck_Prefab;

        [SerializeField]
        Ablaufzyklus ablaufzyklus;

        #endregion

        [SerializeField]
        Analysewerkzeug analysewerkzeug;
        [SerializeField]
        ToggleView toggleView;

        [SerializeField]
        ToggleButton analysewerkzeug_closeButton1;

        [SerializeField]
        ToggleButton analysewerkzeug_closeButton2;

        #region Simulationsdaten aus UI übernehmen

        public void anzahlBurger_Eintragen(int anzahlBurger)
        {
            this.anzahlBurger = anzahlBurger;
        }

        public void anzahlGuter_Eintragen(int anzahlGuter)
        {
            this.anzahlGuter = anzahlGuter;
        }

       public void Lohn_Eintragen(int Lohn_Kreis,int Lohn_Dreieck,int Lohn_Quadrat,int Lohn_Raute,int Lohn_Funfeck)
        {
            this.Lohn_Kreis = Lohn_Kreis;
            this.Lohn_Dreieck = Lohn_Dreieck;
            this.Lohn_Quadrat = Lohn_Quadrat;
            this.Lohn_Raute = Lohn_Raute;
            this.Lohn_Funfeck = Lohn_Funfeck;
        }

        public void geldBesitz_Eintragen(int geldbesitz)
        {
            this.geldbesitz = geldbesitz;
        }

        public void Berufsverteilung_Eintragen(int arbeiter_Kreis, int arbeiter_Dreieck, int arbeiter_Quadrat, int arbeiter_Raute, int arbeiter_Funfeck)
        {
            this.arbeiter_Kreis = arbeiter_Kreis;
            this.arbeiter_Dreieck = arbeiter_Dreieck;
            this.arbeiter_Quadrat = arbeiter_Quadrat;
            this.arbeiter_Raute = arbeiter_Raute;
            this.arbeiter_Funfeck = arbeiter_Funfeck;
        }

        public void Wertvorstellung_Eintragen(int wertvorstellung_Kreis, int wertvorstellung_Dreieck, int wertvorstellung_Quadrat, int wertvorstellung_Raute, int wertvorstellung_Funfeck)
        {
            this.wertvorstellung_Kreis= wertvorstellung_Kreis;
            this.wertvorstellung_Dreieck = wertvorstellung_Dreieck;
            this.wertvorstellung_Quadrat = wertvorstellung_Quadrat;
            this.wertvorstellung_Raute = wertvorstellung_Raute;
            this.wertvorstellung_Funfeck = wertvorstellung_Funfeck;
        }

        public void Gesetze_Eintragen(bool Berufswechel, bool Schulden, bool Erwartungswerte)
        {
            this.berufswechsel = Berufswechel;
            this.schulden = Schulden;
            this.erwartungswerte = Erwartungswerte;
        }
        public void Erwartungswertanpassung_Eintragen(int Erwartungswertanpassung)
        {
            this.erwarungswertanpassung = Erwartungswertanpassung;
        }

        #endregion

        [ExposeMethodInEditorAttribute]
        public void Parameterisieren_Simulation()
        {
            Debug.Log("Die Charts einmal starten, damit die Parameter nicht fehlerhaft sind");
            analysewerkzeug.ToggleVisibility();
            toggleView.ToggleVisibility();
            analysewerkzeug_closeButton1.Toggle();
            analysewerkzeug_closeButton2.Toggle();


            Debug.Log("Die Simulation wird mit den Parametern aus dem UI gestartet");
            siedlung.bauenHäuser(anzahlBurger);
            Erzeuge_Industrie();
            Erzeugen_Gesellschaft();
            ablaufzyklus.ErfasseObjekte();
            ablaufzyklus.erwartungswerte = erwartungswerte;
            marktplatz.GetComponent<Marktplatz>().SchuldenVerbieten = !schulden;
        }

        public void Erzeugen_Bedürfnisse(GameObject Burger)
        {
            if (anzahlGuter >= 1)
            {
                GameObject Kreis = Instantiate(Bedürfnis_Kreis_Prefab, Burger.transform.position, transform.rotation);
                Kreis.name = "Bedürfnis_Kreis";
                Kreis.transform.SetParent(Burger.transform);
                Burger.GetComponent<Burger>().Hinzufügen_Bedurfnis(Kreis.GetComponent<Bedurfnis>());
            }
            if (anzahlGuter >= 2)
            {
                GameObject Dreieck = Instantiate(Bedürfnis_Dreieck_Prefab, Burger.transform.position, transform.rotation);
                Dreieck.name = "Bedürfnis_Dreieck";
                Dreieck.transform.SetParent(Burger.transform);
                Burger.GetComponent<Burger>().Hinzufügen_Bedurfnis(Dreieck.GetComponent<Bedurfnis>());
            }
            if (anzahlGuter >= 3)
            {
                GameObject Quadrat = Instantiate(Bedürfnis_Quadrat_Prefab, Burger.transform.position, transform.rotation);
                Quadrat.name = "Bedürfnis_Quadrat";
                Quadrat.transform.SetParent(Burger.transform);
                Burger.GetComponent<Burger>().Hinzufügen_Bedurfnis(Quadrat.GetComponent<Bedurfnis>());
            }
            if (anzahlGuter >= 4)
            {
                GameObject Raute = Instantiate(Bedürfnis_Raute_Prefab, Burger.transform.position, transform.rotation);
                Raute.name = "Bedürfnis_Raute";
                Raute.transform.SetParent(Burger.transform);
                Burger.GetComponent<Burger>().Hinzufügen_Bedurfnis(Raute.GetComponent<Bedurfnis>());
            }
            if (anzahlGuter >= 5)
            {
                GameObject Funfeck = Instantiate(Bedürfnis_Funfeck_Prefab, Burger.transform.position, transform.rotation);
                Funfeck.name = "Bedürfnis_Fünfeck";
                Funfeck.transform.SetParent(Burger.transform);
                Burger.GetComponent<Burger>().Hinzufügen_Bedurfnis(Funfeck.GetComponent<Bedurfnis>());
            }
        }

        [ExposeMethodInEditorAttribute]
        void Erzeugen_Gesellschaft()
        {
            for (int i = 0; i < anzahlBurger; i++)
            {
                GameObject burger = Instantiate(burgerPrefab);
                burger.transform.parent = gesellschaft.transform;
                burger.name = "Burger " + i;
                Burger b = burger.GetComponent<Burger>();
                b.Erwartungswertanpassung = this.erwarungswertanpassung;
                Erzeugen_Bedürfnisse(burger);

                b.DatenEingeben(Convert.ToInt32(geldbesitz), Convert.ToInt32(wertvorstellung_Kreis), Convert.ToInt32(wertvorstellung_Dreieck), Convert.ToInt32(wertvorstellung_Quadrat), Convert.ToInt32(wertvorstellung_Raute), Convert.ToInt32(wertvorstellung_Funfeck), berufswechsel);

                if (arbeiter_Kreis > 0)
                {
                    b.grundPräferenz = Gut.Güterarten.Kreis;
                    arbeiter_Kreis = arbeiter_Kreis - 1;
                }
                else if(arbeiter_Dreieck > 0)
                {
                    b.grundPräferenz = Gut.Güterarten.Dreieck;
                    arbeiter_Dreieck= arbeiter_Dreieck - 1;
                }
                else if (arbeiter_Quadrat > 0)
                {
                    b.grundPräferenz = Gut.Güterarten.Quadrat;
                    arbeiter_Quadrat = arbeiter_Quadrat - 1;
                }
                else if (arbeiter_Raute > 0)
                {
                    b.grundPräferenz = Gut.Güterarten.Raute;
                    arbeiter_Raute = arbeiter_Raute - 1;
                }
                else if (arbeiter_Funfeck > 0)
                {
                    b.grundPräferenz = Gut.Güterarten.Fünfeck;
                    arbeiter_Funfeck = arbeiter_Funfeck - 1;
                }
            }
        }
        [ExposeMethodInEditorAttribute]
        void Erzeuge_Industrie()
        {
            for (int i = 0; i < anzahlGuter; i++)
            {
                GameObject produktion = Instantiate(produktionPrefab);
                produktion.transform.SetParent(industrie.transform);
                Produktion p = produktion.GetComponent<Produktion>();

                //Noch nicht richtig
                p.anzahlArbeitsplätze = anzahlBurger;

                switch (i)
                {
                    case 0:
                        p.herstellungsGutPrefab = herstellungsgut_Prefab_Kreis;
                        p.lohnProKraft = Lohn_Kreis;
                        produktion.transform.position = produktion.transform.position + new Vector3(40, 10f);
                        p.name = "Produktion - Kreis";
                        break;
                    case 1:
                        p.herstellungsGutPrefab = herstellungsgut_Prefab_Dreieck;
                        p.lohnProKraft = Lohn_Dreieck;
                        produktion.transform.position = produktion.transform.position + new Vector3(80, 10f);
                        p.name = "Produktion - Dreieck";
                        break;
                    case 2:
                        p.herstellungsGutPrefab = herstellungsgut_Prefab_Quadrat;
                        p.lohnProKraft = Lohn_Quadrat;
                        produktion.transform.position = produktion.transform.position + new Vector3(120, 10f);
                        p.name = "Produktion - Quadrat";
                        break;
                    case 3:
                        p.herstellungsGutPrefab = herstellungsgut_Prefab_Raute;
                        p.lohnProKraft = Lohn_Raute;
                        produktion.transform.position = produktion.transform.position + new Vector3(160, 10f);
                        p.name = "Produktion - Raute";
                        break;
                    case 4:
                        p.herstellungsGutPrefab = herstellungsgut_Prefab_Funfeck;
                        p.lohnProKraft = Lohn_Funfeck;
                        produktion.transform.position = produktion.transform.position + new Vector3(200, 10f);
                        p.name = "Produktion - Funfeck";
                        break;
                }
            }
        }
    }
}