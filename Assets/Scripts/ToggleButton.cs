using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleButton : MonoBehaviour
{
    [SerializeField]
    public GameObject button;

    public void Toggle()
    {
        button.SetActive(!button.activeSelf);
    }

}
