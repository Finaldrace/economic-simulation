using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EconomicSimulation

{
    public class Angebot : ScriptableObject
    {
        public enum Angebotsstatus
        {
            Offen,
            Abgelehnt,
            Akzeptiert,
            Abgeschlossen
        }

        public Angebotsstatus Status;
        public Burger Anbieter;
        public Gut Ware;
        public double Angebotspreis;
        public double Minimalpreis;
        void Start()
        {
            Status = Angebotsstatus.Offen;
        }
    }
}