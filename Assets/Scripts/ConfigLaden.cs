using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Michsky.UI.ModernUIPack;
using UnityEngine.EventSystems;
using EconomicSimulation;
using System;

public class ConfigLaden : MonoBehaviour
{
    [Header("Mengen")]
    public Slider anzahlBurger;
    public Slider anzahlGuter;
    public Slider geldbesitz;

    [Header("Produktion")]
    public Slider Lohn_Kreis;
    public Slider Lohn_Dreieck;
    public Slider Lohn_Quadrat;
    public Slider Lohn_Raute;
    public Slider Lohn_Funfeck;

    [Header("Berufsverteilung")]
    public Slider arbeiter_Kreis;
    public Slider arbeiter_Dreieck;
    public Slider arbeiter_Quadrat;
    public Slider arbeiter_Raute;
    public Slider arbeiter_Funfeck;
    
    [Header("Erwartungswerte")]
    public Slider wertvorstellung_Kreis;
    public Slider wertvorstellung_Dreieck;
    public Slider wertvorstellung_Quadrat;
    public Slider wertvorstellung_Raute;
    public Slider wertvorstellung_Funfeck;

    public Slider Erwartungswertanpassung;

    [Header("Gesetze")]
    public Toggle berufswechsel;
    public Toggle schulden;
    public Toggle erwartungswerte;

    public Simulationseinstellung simulationseinstellung;
    public Canvas canvas;
    public CanvasGroup buttonGroup;
    
    public void Simulationrahmen_Verdecken()
    {
        GetComponent<CanvasGroup>().alpha = 0;
    }

    public void simulationStarten()
    {
        simulationseinstellung.anzahlBurger_Eintragen(Convert.ToInt32(anzahlBurger.value));
        simulationseinstellung.anzahlGuter_Eintragen(Convert.ToInt32(anzahlGuter.value));
        simulationseinstellung.Lohn_Eintragen(Convert.ToInt32(Lohn_Kreis.value), Convert.ToInt32(Lohn_Dreieck.value), Convert.ToInt32(Lohn_Quadrat.value), Convert.ToInt32(Lohn_Raute.value), Convert.ToInt32(Lohn_Funfeck.value));

        simulationseinstellung.geldBesitz_Eintragen(Convert.ToInt32(geldbesitz.value));

        simulationseinstellung.Berufsverteilung_Eintragen(Convert.ToInt32(arbeiter_Kreis.value), Convert.ToInt32(arbeiter_Dreieck.value), Convert.ToInt32(arbeiter_Quadrat.value),Convert.ToInt32(arbeiter_Raute.value), Convert.ToInt32(arbeiter_Funfeck.value));

        simulationseinstellung.Wertvorstellung_Eintragen(Convert.ToInt32(wertvorstellung_Kreis.value),Convert.ToInt32(wertvorstellung_Dreieck.value),Convert.ToInt32(wertvorstellung_Quadrat.value),Convert.ToInt32(wertvorstellung_Raute.value),Convert.ToInt32(wertvorstellung_Funfeck.value));

        simulationseinstellung.Gesetze_Eintragen(berufswechsel.isOn, schulden.isOn, erwartungswerte.isOn);
        simulationseinstellung.Erwartungswertanpassung_Eintragen(Convert.ToInt32(Erwartungswertanpassung.value));

        Simulationrahmen_Verdecken();

        simulationseinstellung.Parameterisieren_Simulation();

        buttonGroup.alpha = 1;
    }

    //Jeder Burger geht seiner Arbeit nach und produziert genug, sodass er sich selbst mit dem Gut versorgen und die weiteren verkaufen kann.
    //Da es exakt so viel Produktion und Handel gibt wie ben�tigt wird h�lt sich der Markt im Gleichgewicht.
    //Preise werden nie angepasst
    //Vollkommener Markt

    /* 1# Der perfekte Markt*/
    [ExposeMethodInEditor]
    public void Simulation0()
    {
        /*Mengen*/
        anzahlBurger.value = 3;
        anzahlGuter.value = 3;
        geldbesitz.value = 2;

        /*Produktion*/
        Lohn_Kreis.value = 3;
        Lohn_Dreieck.value = 3;
        Lohn_Quadrat.value = 3;
        Lohn_Raute.value = 0;
        Lohn_Funfeck.value = 0;

        /*Berufsverteilung*/
        arbeiter_Kreis.value = 1;
        arbeiter_Dreieck.value = 1;
        arbeiter_Quadrat.value = 1;
        arbeiter_Raute.value = 0;
        arbeiter_Funfeck.value = 0;

        /*Wertvorstellung*/
        wertvorstellung_Kreis.value = 1;
        wertvorstellung_Dreieck.value = 1;
        wertvorstellung_Quadrat.value = 1;
        wertvorstellung_Raute.value = 0;
        wertvorstellung_Funfeck.value = 0;

        Erwartungswertanpassung.value = 0;

        /*Gesetze*/
        berufswechsel.isOn = false;
        schulden.isOn = true;
        //Die Erwartungswerte sind irrelevant, da die G�ter t�glich stets ideal verkauft werden.
        erwartungswerte.isOn = false;
    }

    //Jeder Mensch versorgt sich selbst, da es keine W�hrung gibt.
    //Es kommt zu keinem Handel
    //Durch den t�glichen Wechsel und die hohen Ertr�ge erh�lt jeder genug G�ter um gl�cklich zu sein.
    //Vergleichbar mit selbstversorgenden Bauernh�fen.

    /* 2# Die Selbstversorger*/
    [ExposeMethodInEditor]
    public void Simulation1()
    {
        /*Mengen*/
        anzahlBurger.value = 100;
        anzahlGuter.value = 2;
        geldbesitz.value = 0;

        /*Produktion*/
        Lohn_Kreis.value = 2;
        Lohn_Dreieck.value = 2;
        Lohn_Quadrat.value = 0;
        Lohn_Raute.value = 0;
        Lohn_Funfeck.value = 0;

        /*Berufsverteilung*/
        arbeiter_Kreis.value = 30;
        arbeiter_Dreieck.value = 70;
        arbeiter_Quadrat.value = 0;
        arbeiter_Raute.value = 0;
        arbeiter_Funfeck.value = 0;

        /*Wertvorstellung*/
        wertvorstellung_Kreis.value = 1;
        wertvorstellung_Dreieck.value = 1;
        wertvorstellung_Quadrat.value = 0;
        wertvorstellung_Raute.value = 0;
        wertvorstellung_Funfeck.value = 0;

        Erwartungswertanpassung.value = 0;

        /*Gesetze*/
        berufswechsel.isOn = true;
        schulden.isOn = false;
        erwartungswerte.isOn = false;
    }

    /* 3# Die Preisentwicklung*/
    [ExposeMethodInEditor]
    public void Simulation2()
    {
        /*Mengen*/
        anzahlBurger.value = 100;
        anzahlGuter.value = 2;
        geldbesitz.value = 100;

        /*Produktion*/
        Lohn_Kreis.value = 3;
        Lohn_Dreieck.value = 2;
        Lohn_Quadrat.value = 0;
        Lohn_Raute.value = 0;
        Lohn_Funfeck.value = 0;

        /*Berufsverteilung*/
        arbeiter_Kreis.value = 20;
        arbeiter_Dreieck.value = 80;
        arbeiter_Quadrat.value = 0;
        arbeiter_Raute.value = 0;
        arbeiter_Funfeck.value = 0;

        /*Wertvorstellung*/
        wertvorstellung_Kreis.value = 50;
        wertvorstellung_Dreieck.value = 50;
        wertvorstellung_Quadrat.value = 0;
        wertvorstellung_Raute.value = 0;
        wertvorstellung_Funfeck.value = 0;

        Erwartungswertanpassung.value = 5;

        /*Gesetze*/
        berufswechsel.isOn = false;
        schulden.isOn = true;
        erwartungswerte.isOn = true;
    }

    /* 4# Die Kreditlosigkeit */
    [ExposeMethodInEditor]
    public void Simulation3()
    {
        /*Mengen*/
        anzahlBurger.value = 100;
        anzahlGuter.value = 4;
        geldbesitz.value = 100;

        /*Produktion*/
        Lohn_Kreis.value = 4;
        Lohn_Dreieck.value = 4;
        Lohn_Quadrat.value = 4;
        Lohn_Raute.value = 4;
        Lohn_Funfeck.value = 0;

        /*Berufsverteilung*/
        arbeiter_Kreis.value = 25;
        arbeiter_Dreieck.value = 25;
        arbeiter_Quadrat.value = 25;
        arbeiter_Raute.value = 25;
        arbeiter_Funfeck.value = 0;

        /*Wertvorstellung*/
        wertvorstellung_Kreis.value = 10;
        wertvorstellung_Dreieck.value = 20; //Teurer als die anderen
        wertvorstellung_Quadrat.value = 10;
        wertvorstellung_Raute.value = 10;
        wertvorstellung_Funfeck.value = 0;

        Erwartungswertanpassung.value = 0;

        /*Gesetze*/
        berufswechsel.isOn = false;
        schulden.isOn = false;
        erwartungswerte.isOn = false;
    }
}
