using System.Collections;
using System.Collections.Generic;
using Com.LuisPedroFonseca.ProCamera2D;
using UnityEngine;

public class ToggleView : MonoBehaviour
{
    [SerializeField]
    public GameObject toggleObject;

    public CanvasGroup canvasGroup;

    public Camera cam;

    public void ToggleVisibility()
    {
        if (canvasGroup.alpha == 1)
        {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
            canvasGroup.interactable = false;
            cam.GetComponent<ProCamera2DPanAndZoom>().DisableOverUGUI = false;
            toggleObject.SetActive(false);
        }
        else
        {
            canvasGroup.alpha = 1;
            canvasGroup.blocksRaycasts = true;
            canvasGroup.interactable = true;
            cam.GetComponent<ProCamera2DPanAndZoom>().DisableOverUGUI = true;
            toggleObject.SetActive(true);
        }
    }
}
