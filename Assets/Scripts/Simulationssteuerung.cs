﻿using System.Collections;
using System.Collections.Generic;
using ChartAndGraph;
using Com.LuisPedroFonseca.ProCamera2D;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EconomicSimulation

{
    public class Simulationssteuerung : MonoBehaviour
    {
        public Ablaufzyklus ablaufzyklus;

        public OutputLog outputLog;

        public ItemLabels itemLabels;
        public ItemLabels itemLabels_Wert;

        public bool SpritesEnabled = false;

        [SerializeField]
        public GameObject Button_Group;

        ProCamera2D procamera2D;

        [SerializeField]
        public NotificationManager notificationManager;
        [SerializeField]
        public CanvasGroup canvasGroup_notificationManager;

        [SerializeField]
        public Marktplatz marktplatz;

        void Start()
        {
            procamera2D = Camera.main.GetComponent<ProCamera2D>();
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
                ablaufzyklus.Wechseln();

            if (Input.GetKeyDown(KeyCode.UpArrow))
                ablaufzyklus.Starten();

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                outputLog.ClearMessages();
                notificationManager.title = "Transaktionslog - Leeren";
                notificationManager.description = "Der Transaktionslog wurde geleert.";
                notificationManager.UpdateUI();
                notificationManager.OpenNotification();
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                ablaufzyklus.Schnelldurchlauf_Tag();
            }

            if (Input.GetKeyDown(KeyCode.F1))
            {
                Button_Group.SetActive(!Button_Group.activeSelf);
                if (Button_Group.activeSelf == false)
                {
                    notificationManager.title = "Steuermenü - Ansicht";
                    notificationManager.description = "Das Steuermenü ausblenden";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
                else
                {
                    notificationManager.title = "Steuermenü - Ansicht";
                    notificationManager.description = "Das Steuermenü einblenden";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                    
                }

            }

            if (Input.GetKeyDown(KeyCode.F2))
            {
                itemLabels.enabled = !itemLabels.enabled;
                itemLabels_Wert.enabled = !itemLabels_Wert.enabled;

                if (itemLabels.enabled == false)
                {
                    notificationManager.title = "Graphen - Wertbeschriftung";
                    notificationManager.description = "Texte der Messpunkte ausblenden";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
                else
                {
                    notificationManager.title = "Graphen - Wertbeschriftung";
                    notificationManager.description = "Texte der Messpunkte einblenden";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
            }

            if (Input.GetKeyDown(KeyCode.F3))
            {
                GameObject[] Guter = GameObject.FindGameObjectsWithTag("Gut");
                SpritesEnabled = !SpritesEnabled;
                foreach (GameObject gut in Guter)
                {
                    gut.GetComponent<SpriteRenderer>().enabled = SpritesEnabled;
                }

                if (SpritesEnabled == true)
                {
                    notificationManager.title = "Güter - Sprites";
                    notificationManager.description = "Güteransicht aktivieren";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
                else
                {
                    notificationManager.title = "Güter - Sprites";
                    notificationManager.description = "Güteransicht deaktivieren";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();

                }

            }

            if (Input.GetKeyDown(KeyCode.F4))
            {
                GameObject[] Burgers= GameObject.FindGameObjectsWithTag("Burger");
                SpritesEnabled = !SpritesEnabled;
                foreach (GameObject Burger in Burgers)
                {
                    Burger.GetComponent<Burger>().GeldEinblenden = !Burger.GetComponent<Burger>().GeldEinblenden; //.GetComponent<SpriteRenderer>().enabled = SpritesEnabled;
                }

                if (SpritesEnabled == true)
                {
                    notificationManager.title = "Geldmenge Bürger - Wert";
                    notificationManager.description = "Geldmengenansicht aktivieren";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
                else
                {
                    notificationManager.title = "Geldmenge Bürger - Wert";
                    notificationManager.description = "Geldmengenansicht deaktivieren";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
            }

            if (Input.GetKeyDown(KeyCode.F5))
            {
                procamera2D.GetComponent<ProCamera2DPanAndZoom>().enabled = false;
                Camera.main.GetComponent<ProCamera2D>().MoveCameraInstantlyToPosition(new Vector2(0, 0));
                procamera2D.GetComponent<ProCamera2DPanAndZoom>().enabled = true;
                //GameObject.FindGameObjectWithTag("MainCamera")

                notificationManager.title = "Kamera - Reset";
                notificationManager.description = "Die Kameraposition wurde auf den Startpunkt gesetzt";
                notificationManager.UpdateUI();
                notificationManager.OpenNotification();

            }

            //Benachrichtigungen Aktivieren/Deaktivieren
            if (Input.GetKeyDown(KeyCode.F6))
            {
                if (canvasGroup_notificationManager.alpha == 0)
                {
                    canvasGroup_notificationManager.alpha = 1;
                    canvasGroup_notificationManager.GetComponent<Animator>().enabled = true;

                    notificationManager.title = "Benachrichtigungen - Aktiviert";
                    notificationManager.description = "Die Benachrichtigungen wurden reaktiviert";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
                else
                {
                    canvasGroup_notificationManager.alpha = 0;
                    canvasGroup_notificationManager.GetComponent<Animator>().enabled = false;
                }
            }

            if (Input.GetKeyDown(KeyCode.F7))
            {
                GameObject[] Burgers = GameObject.FindGameObjectsWithTag("Burger");
                bool berufswechsel = Burgers[0].GetComponent<Burger>().berufswechsel; 

                foreach (GameObject Burger in Burgers)
                {
                    Burger.GetComponent<Burger>().berufswechsel = !Burger.GetComponent<Burger>().berufswechsel;
                }

                if (!berufswechsel == true)
                {
                    notificationManager.title = "Gesetz - Berufswechsel";
                    notificationManager.description = "Berufswechsel erlauben";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
                else
                {
                    notificationManager.title = "Gesetz - Berufswechsel";
                    notificationManager.description = "Berufswechsel verbieten";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
            }

            if (Input.GetKeyDown(KeyCode.F8))
            {
                marktplatz.SchuldenVerbieten = !marktplatz.SchuldenVerbieten;

                if (marktplatz.SchuldenVerbieten  == false)
                {
                    notificationManager.title = "Gesetz - Schulden";
                    notificationManager.description = "Schulden erlauben";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
                else
                {
                    notificationManager.title = "Gesetz - Schulden";
                    notificationManager.description = "Schulden verbieten";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
            }

            if (Input.GetKeyDown(KeyCode.F9))
            {
                ablaufzyklus.erwartungswerte = !ablaufzyklus.erwartungswerte;

                if (ablaufzyklus.erwartungswerte == true)
                {
                    notificationManager.title = "Gesetz - Erwartungswert";
                    notificationManager.description = "Preisanpassung erlauben";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
                else
                {
                    notificationManager.title = "Gesetz - Erwartungswert";
                    notificationManager.description = "Preisanpassung verbieten";
                    notificationManager.UpdateUI();
                    notificationManager.OpenNotification();
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Scene scene = SceneManager.GetActiveScene();
                SceneManager.LoadScene(scene.name);
            }
        }

        public void Software_Beenden()
        {
            Application.Quit();
        }
    }

}