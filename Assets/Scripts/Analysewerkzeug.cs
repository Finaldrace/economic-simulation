﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;
using TMPro;
using System.Linq;
using System;

public class Analysewerkzeug : MonoBehaviour
{
    public CanvasGroup canvasGroup;

    [Header("Statistiken")]
    [SerializeField]
    public float Zufriedenheit_Durchschnitt;
    [SerializeField]
    public float GuterBesitz_Durchschnitt;
    [SerializeField]
    public float GeldBesitz_Durchschnitt;
    [SerializeField]
    public float Wahrnehmung_Kreis_Durchschnitt;
    [SerializeField]
    public float Wahrnehmung_Dreieck_Durchschnitt;
    [SerializeField]
    public float Wahrnehmung_Quadrat_Durchschnitt;
    [SerializeField]
    public float Wahrnehmung_Raute_Durchschnitt;
    [SerializeField]
    public float Wahrnehmung_Funfeck_Durchschnitt;
    [SerializeField]
    public float Median_Geldbesitz;

    [Header("Tagessummen")]
    [SerializeField]
    public float Zufriedenheit_Tagessumme;
    [SerializeField]
    public float GuterBesitz_Tagessumme;
    [SerializeField]
    public float GeldBesitz_Tagessumme;

    [SerializeField]
    public float W_Kreis_Summe;
    [SerializeField]
    public float W_Dreieck_Summe;
    [SerializeField]
    public float W_Quadrat_Summe;
    [SerializeField]
    public float W_Raute_Summe;
    [SerializeField]
    public float W_Funfeck_Summe;

    [Header("Verlinkungen")]
    public GameObject Graph;

    [SerializeField]
    GraphChart_Datenschnittstelle graphChart_Datenschnittstelle;
    [SerializeField]
    GraphChart_Datenschnittstelle_Werte graphChart_Datenschnittstelle_Werte;

    [SerializeField]
    TextMeshPro GeldmengeText;

    public Camera cam;

    public List<float> GeldBesitztümer_EinzelWerte;
    public float Erfasste_Besitztumer_Absolut;
    public float Erfasste_Vorstellungen;

    void Start()
    {
        canvasGroup.GetComponent<CanvasGroup>();
        GeldBesitztümer_EinzelWerte = new List<float>();
    }

    public void Erfasse_BurgerBesitz(float Zufriedenheit, float GuterBesitz, float GeldBesitz)
    {
        Zufriedenheit_Tagessumme = Zufriedenheit_Tagessumme + Zufriedenheit;
        GuterBesitz_Tagessumme = GuterBesitz_Tagessumme + GuterBesitz;
        GeldBesitz_Tagessumme = GeldBesitz_Tagessumme + GeldBesitz;

        GeldBesitztümer_EinzelWerte.Add(GeldBesitz);

        Erfasste_Besitztumer_Absolut = Erfasste_Besitztumer_Absolut + 1;
    }

    public void Erfasse_BurgerVorstellungen(float W_Kreis, float W_Dreieck, float W_Quadrat, float W_Raute, float W_Funfeck)
    {
        W_Kreis_Summe = W_Kreis_Summe + W_Kreis;
        W_Dreieck_Summe = W_Dreieck_Summe + W_Dreieck;
        W_Quadrat_Summe = W_Quadrat_Summe + W_Quadrat;
        W_Raute_Summe = W_Raute_Summe + W_Raute;
        W_Funfeck_Summe = W_Funfeck_Summe + W_Funfeck;

        Erfasste_Vorstellungen = Erfasste_Vorstellungen + 1;
    }

    public void BerechneDurchschnitte()
    {
        Zufriedenheit_Durchschnitt = Zufriedenheit_Tagessumme / Erfasste_Besitztumer_Absolut;
        GuterBesitz_Durchschnitt = GuterBesitz_Tagessumme / Erfasste_Besitztumer_Absolut;
        GeldBesitz_Durchschnitt = GeldBesitz_Tagessumme / Erfasste_Besitztumer_Absolut;    
        Wahrnehmung_Kreis_Durchschnitt = W_Kreis_Summe / Erfasste_Vorstellungen;
        Wahrnehmung_Dreieck_Durchschnitt = W_Dreieck_Summe / Erfasste_Vorstellungen;
        Wahrnehmung_Quadrat_Durchschnitt = W_Quadrat_Summe / Erfasste_Vorstellungen;
        Wahrnehmung_Raute_Durchschnitt = W_Raute_Summe / Erfasste_Vorstellungen;
        Wahrnehmung_Funfeck_Durchschnitt = W_Funfeck_Summe / Erfasste_Vorstellungen;

        GeldMedianBerechnen();
    }

    public static double getMedian(float[] array)
    {
        int zaehlen = array.GetLength(0);
        if (zaehlen == 0) return (0);
        Array.Sort(array);
        if ((zaehlen % 2) == 0)
            return ((array[(int)(zaehlen / 2) - 1] + array[(int)(zaehlen / 2)]) / 2);
        else
            return (array[zaehlen / 2]);
    }

    public void GeldMedianBerechnen()
    {
        float[] werte = GeldBesitztümer_EinzelWerte.ToArray();
        Median_Geldbesitz = (float)getMedian(werte);
        GeldBesitztümer_EinzelWerte.Clear();
    }

    public void BefullenStatistik()
    {
        graphChart_Datenschnittstelle.AddDay(Zufriedenheit_Durchschnitt * 100, GuterBesitz_Durchschnitt, Median_Geldbesitz);
        graphChart_Datenschnittstelle_Werte.AddDay(Wahrnehmung_Kreis_Durchschnitt, Wahrnehmung_Dreieck_Durchschnitt, Wahrnehmung_Quadrat_Durchschnitt, Wahrnehmung_Raute_Durchschnitt, Wahrnehmung_Funfeck_Durchschnitt);
        GeldmengeText.text = "Geldmenge: " + GeldBesitz_Tagessumme.ToString(); 
    }

    public void Reset_Tag()
    {
        Zufriedenheit_Tagessumme = 0;
        GuterBesitz_Tagessumme = 0;
        GeldBesitz_Tagessumme = 0;

        W_Kreis_Summe = 0;
        W_Dreieck_Summe = 0;
        W_Quadrat_Summe = 0;
        W_Raute_Summe = 0;
        W_Funfeck_Summe = 0;

        Erfasste_Besitztumer_Absolut = 0;
        Erfasste_Vorstellungen = 0;
    }

    public void ToggleVisibility()
    {
        if (canvasGroup.alpha == 1)
        {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
            canvasGroup.interactable = false;
            cam.GetComponent<ProCamera2DPanAndZoom>().DisableOverUGUI = false;
            Graph.SetActive(false);
        }
        else
        {
            canvasGroup.alpha = 1;
            canvasGroup.blocksRaycasts = true;
            canvasGroup.interactable = true;
            cam.GetComponent<ProCamera2DPanAndZoom>().DisableOverUGUI = true;
            Graph.SetActive(true);
        }
    }
}
