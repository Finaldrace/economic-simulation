#define Graph_And_Chart_PRO
using UnityEngine;
using ChartAndGraph;
using System.Collections;
using System;

public class GraphChart_Datenschnittstelle_Werte : MonoBehaviour
{
    [SerializeField]
    public int days;

    GraphChartBase graph;
    void Start ()
    {
        days = 1;
        graph = GetComponent<GraphChartBase>();

        if (graph != null)
        {
            graph.Scrollable = false;
            graph.HorizontalValueToStringMap[0.0] = "Zero"; // example of how to set custom axis strings
            graph.DataSource.StartBatch();
            //graph.DataSource.ClearCategory("Player 1");
            //graph.DataSource.ClearAndMakeBezierCurve("Player 2");

            graph.DataSource.ClearCategory("Kreis");
            graph.DataSource.ClearAndMakeBezierCurve("Dreieck");
            graph.DataSource.ClearAndMakeBezierCurve("Quadrat");
            graph.DataSource.ClearAndMakeBezierCurve("Raute");
            graph.DataSource.ClearAndMakeBezierCurve("Funfeck");

            graph.DataSource.SetCurveInitialPoint("Dreieck", 0, 0);
            graph.DataSource.SetCurveInitialPoint("Quadrat", 0, 0);
            graph.DataSource.SetCurveInitialPoint("Raute", 0, 0);
            graph.DataSource.SetCurveInitialPoint("Funfeck", 0, 0);
            /*
            for (int i = 0; i <5; i++)
            {
                graph.DataSource.AddPointToCategory("Zufriedenheit", i*5,Random.value*10f + 20f);
                if (i == 0)
                    graph.DataSource.SetCurveInitialPoint("G�ter", i*5, Random.value * 10f + 10f);
                else
                    graph.DataSource.AddLinearCurveToCategory("G�ter", 
                                                                    new DoubleVector2(i*5 , Random.value * 10f + 10f));
            }
            */
            //graph.DataSource.MakeCurveCategorySmooth("G�ter");
            graph.DataSource.EndBatch();
        }
       // StartCoroutine(ClearAll());
    }
    public void AddDay(float Kreis, float Dreieck, float Quadrat, float Raute, float Funfeck)
    {
        graph.DataSource.AddPointToCategory("Kreis", days, Convert.ToDouble(Kreis));
        graph.DataSource.AddLinearCurveToCategory("Dreieck",new DoubleVector2(days, Convert.ToDouble(Dreieck)));
        graph.DataSource.AddLinearCurveToCategory("Quadrat", new DoubleVector2(days, Convert.ToDouble(Quadrat)));
        graph.DataSource.AddLinearCurveToCategory("Raute", new DoubleVector2(days, Convert.ToDouble(Raute)));
        graph.DataSource.AddLinearCurveToCategory("Funfeck", new DoubleVector2(days, Convert.ToDouble(Funfeck)));

        days = days + 1;
    }

    [ExposeMethodInEditor]
    [Obsolete]
    public void AddDayTest()
    {
        graph.DataSource.AddPointToCategory("Kreis", days, UnityEngine.Random.RandomRange(30f,100f));
        graph.DataSource.AddLinearCurveToCategory("Dreieck", new DoubleVector2(days, Convert.ToDouble(UnityEngine.Random.RandomRange(0f, 50f))));
        graph.DataSource.AddLinearCurveToCategory("Quadrat", new DoubleVector2(days, Convert.ToDouble(UnityEngine.Random.RandomRange(10f,30f))));
        graph.DataSource.AddLinearCurveToCategory("Raute", new DoubleVector2(days, Convert.ToDouble(UnityEngine.Random.RandomRange(10f, 30f))));
        graph.DataSource.AddLinearCurveToCategory("Funfeck", new DoubleVector2(days, Convert.ToDouble(UnityEngine.Random.RandomRange(10f, 30f))));

        days = days + 1;
    }

    [ExposeMethodInEditor]
    IEnumerator ClearAll()
    {
        yield return new WaitForSeconds(5f);
        GraphChartBase graph = GetComponent<GraphChartBase>();

        graph.DataSource.Clear();
    }
}
