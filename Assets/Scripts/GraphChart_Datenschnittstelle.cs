#define Graph_And_Chart_PRO
using UnityEngine;
using ChartAndGraph;
using System.Collections;
using System;

public class GraphChart_Datenschnittstelle : MonoBehaviour
{
    [SerializeField]
    public int days;

    GraphChartBase graph;
    void Start ()
    {
        days = 1;
        graph = GetComponent<GraphChartBase>();

        if (graph != null)
        {
            graph.Scrollable = false;
            graph.HorizontalValueToStringMap[0.0] = "Zero"; // example of how to set custom axis strings
            graph.DataSource.StartBatch();
            //graph.DataSource.ClearCategory("Player 1");
            //graph.DataSource.ClearAndMakeBezierCurve("Player 2");

            graph.DataSource.ClearCategory("Zufriedenheit");
            graph.DataSource.ClearAndMakeBezierCurve("G�ter");
            graph.DataSource.ClearAndMakeBezierCurve("Geld");

            graph.DataSource.SetCurveInitialPoint("G�ter", 0, 0);

            graph.DataSource.SetCurveInitialPoint("Geld", 0, 0);
            /*
            for (int i = 0; i <5; i++)
            {
                graph.DataSource.AddPointToCategory("Zufriedenheit", i*5,Random.value*10f + 20f);
                if (i == 0)
                    graph.DataSource.SetCurveInitialPoint("G�ter", i*5, Random.value * 10f + 10f);
                else
                    graph.DataSource.AddLinearCurveToCategory("G�ter", 
                                                                    new DoubleVector2(i*5 , Random.value * 10f + 10f));
            }
            */
            //graph.DataSource.MakeCurveCategorySmooth("G�ter");
            graph.DataSource.EndBatch();
        }
       // StartCoroutine(ClearAll());
    }

    public void AddDay(float Zufriedenheit, float Guterbesitz, float GeldBesitz)
    {
        graph.DataSource.AddPointToCategory("Zufriedenheit", days, Convert.ToDouble(Zufriedenheit));
        graph.DataSource.AddLinearCurveToCategory("G�ter",new DoubleVector2(days, Convert.ToDouble(Guterbesitz)));
        graph.DataSource.AddLinearCurveToCategory("Geld", new DoubleVector2(days, Convert.ToDouble(GeldBesitz)));

        days = days + 1;
    }

    [ExposeMethodInEditor]
    [Obsolete]
    public void AddDayTest()
    {
        graph.DataSource.AddPointToCategory("Zufriedenheit", days, UnityEngine.Random.RandomRange(30f,100f));
        graph.DataSource.AddLinearCurveToCategory("G�ter", new DoubleVector2(days, Convert.ToDouble(UnityEngine.Random.RandomRange(0f, 50f))));
        //graph.DataSource.AddPointToCategory("G�ter", days, Random.RandomRange(0f, 100f));
        graph.DataSource.AddLinearCurveToCategory("Geld", new DoubleVector2(days, Convert.ToDouble(UnityEngine.Random.RandomRange(10f,30f))));
        //graph.DataSource.AddPointToCategory("Geld", days, Random.RandomRange(0f, 100f));
        days = days + 1;
    }

    [ExposeMethodInEditor]
    IEnumerator ClearAll()
    {
        yield return new WaitForSeconds(5f);
        GraphChartBase graph = GetComponent<GraphChartBase>();

        graph.DataSource.Clear();
    }
}
