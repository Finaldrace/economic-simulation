﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EconomicSimulation
{
    public class Bedurfnis : MonoBehaviour
    {
        [SerializeField]
        public Gut.Güterarten gut;
        [SerializeField]
        public int menge;

        [SerializeField]
        public int konsumiert;

        [SerializeField]
        public int gewichtung;
    }
}
